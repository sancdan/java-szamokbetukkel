
package kisebbiknegyzetgyoke;


public class KisebbikNegyzetgyoke {

    public KisebbikNegyzetgyoke(String szam1, String szam2){
        int szam;
        int x = Integer.parseInt(szam1);
        int y = Integer.parseInt(szam2);
        
        if(x-y < 0)
            szam = x;
        else
            szam = y;

        double negyzetgyok = Math.sqrt(szam);
        
        System.out.println("A(z) " + szam + " a kisebb, melynek négyzetgyöke: " + negyzetgyok);
    }
    
}
