
package oszthato;


public class Oszthato {
    
    public Oszthato(){
        
    }
    
    public Oszthato(int szam){
        if(szam % 2 == 0)
            System.out.println("Ez a szám osztható kettővel");
        else
            System.out.println("Ez a szám nem osztható kettővel");
        
        if(szam % 3 == 0)
            System.out.println("Ez a szám osztható hárommal");
        else
            System.out.println("Ez a szám nem osztható hárommal");
    }
    
    public void oszthatoOttelDeHuszonottelNem(){
        for(int i = 1; i<=200; i++){
            if(i % 5 == 0 && i % 25 != 0)
                System.out.println(i);
        }
    }
    
}
