
package main;

import szamokbetukkel.SzamokBetukkel;
import jonapot.JoNapot;
import negyzetreemeles.NegyzetreEmel;
import oszthato.Oszthato;
import kisebbiknegyzetgyoke.KisebbikNegyzetgyoke;
import erdemjegy.ErdemjegyBetuvel;
import tizedesvesszo.Tizedesvesszo;

public class Main {

    public static void main(String[] args) {

        
        //kiírja az összes kétjegyű számot
        for( int i = 10; i<=99; i++ ){
            SzamokBetukkel sz = new SzamokBetukkel(i);
            System.out.println(sz.konvertal());
        }
            
        // ha nem kétjegyű számot adunk meg akkor nem hajtja végre
        // és figyelmeztet
        SzamokBetukkel k = new SzamokBetukkel(128);
        System.out.println(k.konvertal());

        
        //------------------------------------------------------------------------------------
        System.out.println("-----------------------------------------------");
        JoNapot j = new JoNapot();
        for( int i = 1; i<=20; i++ ){
            System.out.println(i + ". " + j.irdKiHogyJoNapot());
        }

        
        //------------------------------------------------------------------------------------
        System.out.println("-----------------------------------------------");
        NegyzetreEmel n = new NegyzetreEmel();
        

        //------------------------------------------------------------------------------------
        System.out.println("-----------------------------------------------");
        Oszthato o = new Oszthato(434332);
        

        //------------------------------------------------------------------------------------
        System.out.println("-----------------------------------------------");
        Oszthato h = new Oszthato();
        h.oszthatoOttelDeHuszonottelNem();
        

        //------------------------------------------------------------------------------------
        System.out.println("-----------------------------------------------");
        KisebbikNegyzetgyoke l = new KisebbikNegyzetgyoke("984","432");
        
        
        //------------------------------------------------------------------------------------
        System.out.println("-----------------------------------------------");
        ErdemjegyBetuvel erdemjegy1 = new ErdemjegyBetuvel(1);
        ErdemjegyBetuvel erdemjegy2 = new ErdemjegyBetuvel(2);
        ErdemjegyBetuvel erdemjegy3 = new ErdemjegyBetuvel(3);
        ErdemjegyBetuvel erdemjegy4 = new ErdemjegyBetuvel(4);
        ErdemjegyBetuvel erdemjegy5 = new ErdemjegyBetuvel(5);
        
        
        //------------------------------------------------------------------------------------
        System.out.println("-----------------------------------------------");
        Tizedesvesszo t = new Tizedesvesszo(5.43434);
        
    } //main
    
} // class
