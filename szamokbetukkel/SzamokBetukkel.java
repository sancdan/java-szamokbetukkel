
package szamokbetukkel;


public class SzamokBetukkel {

    private int szam;

    
    public SzamokBetukkel(int sz) {
        this.szam = sz;
    }
    
    // megállapítjuk, hogy kétjegyű-e a szám
    public boolean isKetjegyu(){
        if(String.valueOf(this.szam).length() == 2){
            return true;
        }
        return false;
    }
    
    // megállapítjuk az első számjegyet
    public int getElsoSzamjegy(){
        return this.szam/10;
    }
    
    // megállapítjuk a második számjegyet
    // maradékos osztás segítségével
    // elosztjuk a számot tízzel és a maradék
    // a második számjegy
    public int getMasodikSzamjegy(){
        return this.szam % 10; 
        //pl. ha a szám 46, akkor a visszatérési érték ebben az esetben 6 lesz
    }
    
    // az első számjegy alapján beállítjuk a tizes változó értékét
    // a szöveges megfelelőjére
    // a kerek tíz és kerek huszas számokkal gond van, mert azt máshogy írjuk,
    // mintha nem kerek lenne ezért ezzel trükközni kell egy kicsit
    public String getTizes(){
        String tizes = "";
        int elsoSzamjegy = this.getElsoSzamjegy();
        
        // ha az első számjegy 1 vagy 2 és a második számjegy 0
        if(elsoSzamjegy < 3 && this.getMasodikSzamjegy() == 0){
            elsoSzamjegy = elsoSzamjegy + (elsoSzamjegy * 9);
            // így elsoSzamjegy = 1 + (1 * 9) = 10;
            // így elsoSzamjegy = 2 + (2 * 9) = 20;
        }
        
        switch(elsoSzamjegy){
            case 10: 
                tizes = "Tíz";
                break;
            case 20: 
                tizes = "Húsz";
                break;
            case 1: 
                tizes = "Tizen";
                break;
            case 2: 
                tizes = "Huszon";
                break;
            case 3: 
                tizes = "Harminc";
                break;
            case 4: 
                tizes = "Negyven";
                break;
            case 5: 
                tizes = "Ötven";
                break;
            case 6: 
                tizes = "Hatvan";
                break;
            case 7: 
                tizes = "Hetven";
                break;
            case 8: 
                tizes = "Nyolcvan";
                break;
            case 9: 
                tizes = "Kilencven";
                break;
        }
        return tizes;
    }
    
    public String getEgyes(){
        String egyes = "";
        switch(this.getMasodikSzamjegy()){
            case 1: 
                egyes = "egy";
                break;
            case 2: 
                egyes = "kettő";
                break;
            case 3: 
                egyes = "három";
                break;
            case 4: 
                egyes = "négy";
                break;
            case 5: 
                egyes = "öt";
                break;
            case 6: 
                egyes = "hat";
                break;
            case 7: 
                egyes = "hét";
                break;
            case 8: 
                egyes = "nyolc";
                break;
            case 9: 
                egyes = "kilenc";
                break;
                
        }
        return egyes;
        
    }
    
    
   public String konvertal(){
        if(this.isKetjegyu()){
            return getTizes() + getEgyes();
        }else{
            return "A(z) " + this.szam + " nem kétjegyű!";
        }
   }
    
    
        
}
