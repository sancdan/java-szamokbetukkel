
package tizedesvesszo;


public class Tizedesvesszo {

    public Tizedesvesszo(double szam){
        // 1. megoldás
        String sz = String.valueOf(szam);
        System.out.println(sz.replace(".", ","));
        
        // 2. megoldás
        Double dd = new Double(szam);
        String s = dd.toString();
        System.out.println(s.replace(".", ","));
    }
    
}
